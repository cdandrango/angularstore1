import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appTrackerclick]'
})
export class TrackerclickDirective {
private element:HTMLInputElement;
  constructor(private elref:ElementRef) { 
this.element=elref.nativeElement;
fromEvent(this.element,"click").subscribe(d=>this.track(d));

  }
track(evento:Event){
const eleme=this.element.attributes.getNamedItem('ele').value.split(' ');
console.log(`s ${eleme}`);
}
}
