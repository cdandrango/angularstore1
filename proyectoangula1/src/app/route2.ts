import { Routes } from '@angular/router';
import { VuelosDetalleComponentComponent } from './component/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { VuelosMainComponentComponent } from './component/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasinfoComponentComponent } from './component/vuelos/vuelos-masinfo-component/vuelos-masinfo-component.component';
import { VueloscomponentComponent } from './component/vuelos/vueloscomponent/vueloscomponent.component';

export const childrenRoutesVuelos: Routes = [
    { path: '', redirectTo: 'main', pathMatch: 'full' },
    { path: 'main', component: VuelosMainComponentComponent },
    { path: 'mas-info', component: VuelosMasinfoComponentComponent },
    { path: ':id', component:  VuelosDetalleComponentComponent },
  ];
 
  