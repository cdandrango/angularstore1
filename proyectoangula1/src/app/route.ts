import { Routes } from '@angular/router';
import { HeaderComponent } from './component/header/header.component';
import { LoginComponent } from './component/login/login/login.component';
import { NuevoComponent } from './component/nuevo/nuevo.component';
import { ProtectedComponent } from './component/protected/protected/protected.component';
import { VueloscomponentComponent } from './component/vuelos/vueloscomponent/vueloscomponent.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { childrenRoutesVuelos } from './route2';
import { NgModule } from '@angular/core';

export const routes: Routes = [ { path: '', pathMatch: 'full', redirectTo: 'Inicio' },{ path: 'destino/:id', component: NuevoComponent},{ path: 'Inicio', component: HeaderComponent},{path: 'Login', component: LoginComponent},{ path: 'protected',
component: ProtectedComponent,
canActivate: [ UsuarioLogueadoGuard ]}, {path: 'vuelos',
component: VueloscomponentComponent,
canActivate: [ UsuarioLogueadoGuard ],
children: childrenRoutesVuelos}]

