import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { ProviderAst } from '@angular/compiler';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { AppConfig, AppState, APP_CONFIG, MyDatabase } from '../app.module';
import { datosviaje } from './datos_viaje';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destino_static';


@Injectable()
export class DestinosApiClient {
destinos:datosviaje[]=[];
db:any;
constructor(@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
private http: HttpClient,private store: Store<AppState>) {
   this.db=new MyDatabase();
}
getById(id: String): datosviaje {
  return this.destinos.filter((d)=> { return d.id.toString() === id; })[0];
  
  }
add(d:datosviaje){

  const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
  const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
  this.http.request(req).subscribe((data: HttpResponse<{}>) => {
    if (data.status === 200) {
      this.store.dispatch(new NuevoDestinoAction(d));
      const myDb = this.db;
      myDb.destinos.add(d);
      console.log(myDb);
      console.log('todos los destinos de la db!');
      myDb.destinos.toArray().then(destinos => console.log(destinos))
    }
    });
  }

elegir(e:datosviaje){
    this.store.dispatch(new ElegidoFavoritoAction(e));


}

}