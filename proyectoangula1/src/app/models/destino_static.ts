import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { datosviaje } from './datos_viaje';
import { DestinosApiClient } from './destino_client';
import { HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

// ESTADO
export interface DestinosViajesState {
    items: datosviaje[];
    
    favorito: datosviaje;
}

export function intializeDestinosViajesState() {
  return {
    items: [],
 
    favorito: null
  };
}

// ACCIONES
export enum DestinosViajesActionTypes {
  NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
  ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
  BORRADO= '[Destinos Viajes] Borrado',
  VOTOPOSITIVO='[Destinos Viajes] Votopositivo',
  VOTONEGATIVO='[Destinos Viajes] Votonegativo',
  RESET='[Destinos Viajes] Reset',
  INIT_MY_DATA = '[Destinos Viajes] Init My Data'
}

export class NuevoDestinoAction implements Action {
  type = DestinosViajesActionTypes.NUEVO_DESTINO;
  constructor(public destino: datosviaje) {}
}

export class ElegidoFavoritoAction implements Action {
  type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
  constructor(public destino: datosviaje) {}
}
export class BorradoAction implements Action {
  type = DestinosViajesActionTypes.BORRADO;
  constructor(public destino: datosviaje) {}
}
export class Votopositivo implements Action {
  type = DestinosViajesActionTypes.VOTOPOSITIVO;
  constructor(public destino: datosviaje) {}
}
export class Votonegativo implements Action {
  type = DestinosViajesActionTypes.VOTONEGATIVO;
  constructor(public destino: datosviaje) {}
}
export class Reset implements Action {
  type = DestinosViajesActionTypes.RESET;
  constructor(public destino: datosviaje) {}
}
export class InitMyDataAction implements Action {
  type = DestinosViajesActionTypes.INIT_MY_DATA;
  constructor(public destino: datosviaje) {}
}


export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction| Votopositivo | Votonegativo | BorradoAction

// REDUCERS
export function reducerDestinosViajes(
  state: DestinosViajesState,
  action: DestinosViajesActions
): DestinosViajesState {
  switch (action.type) {
    case DestinosViajesActionTypes.INIT_MY_DATA: {
      let destinos;
      destinos=(action as InitMyDataAction).destino;
      return {
          ...state,
          items: destinos.map((d) => new datosviaje(d, ''))
        };
    }
    case DestinosViajesActionTypes.NUEVO_DESTINO: {
      return {
          ...state,
          items: [...state.items, (action as NuevoDestinoAction).destino ]
        };
    }
    case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
        state.items.forEach(x => x.setSelect(false));
        const fav: datosviaje = (action as ElegidoFavoritoAction).destino;
        fav.setSelect(true);
        return {
          ...state,
          favorito: fav
        };
    }
    case DestinosViajesActionTypes.BORRADO: {
    
      const fav: datosviaje = (action as ElegidoFavoritoAction).destino;
     let y=0;
     var s=0;
      fav.setSelect(true);
      state.items.forEach(x =>  {if(x==fav){
s=y
      }
 y++     
      });
      state.items.splice(s,1);
      console.log(state.items);
      return {...state};
  }
  case DestinosViajesActionTypes.VOTOPOSITIVO: {
    const d: datosviaje = (action as ElegidoFavoritoAction).destino;
    d.votospositivo();
    return {
        ...state,
      };
  } 
  case DestinosViajesActionTypes.VOTONEGATIVO: {
    const d: datosviaje = (action as ElegidoFavoritoAction).destino;
    d.votonegativo();
    return {
        ...state,
      };
  } 
  case DestinosViajesActionTypes.RESET: {
    const d: datosviaje = (action as ElegidoFavoritoAction).destino;
    d.reset();
    return {
        ...state,
      };
  } 
}


  return state;
}

// EFFECTS
@Injectable()
export class DestinosViajesEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
    map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
  );

  constructor(private actions$: Actions) {}
}