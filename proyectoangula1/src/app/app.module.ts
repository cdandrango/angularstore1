import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import {routes} from './route'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { DestinoviajeComponent } from './component/destinoviaje/destinoviaje.component';
import { MenuComponent } from './component/menu/menu.component';
import { NuevoComponent } from './component/nuevo/nuevo.component';
import { Router, RouterModule } from '@angular/router';
import { FormnuevoComponent } from './component/formnuevo/formnuevo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DestinosApiClient } from './models/destino_client';
import { DestinosViajesEffects, DestinosViajesState, InitMyDataAction, intializeDestinosViajesState, reducerDestinosViajes } from './models/destino_static';
import { ActionReducerMap, Store } from '@ngrx/store';
import { StoreModule as NgRxStoreModule } from '@ngrx/store';
import{EffectsModule} from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { _runtimeChecksFactory } from '@ngrx/store/src/runtime_checks';
import { LoginComponent } from './component/login/login/login.component';
import { ProtectedComponent } from './component/protected/protected/protected.component';
import { VueloscomponentComponent } from './component/vuelos/vueloscomponent/vueloscomponent.component';
import { VuelosMainComponentComponent } from './component/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasinfoComponentComponent } from './component/vuelos/vuelos-masinfo-component/vuelos-masinfo-component.component';
import { VuelosDetalleComponentComponent } from './component/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { childrenRoutesVuelos } from './route2';
import { ReservasModule } from './reservas/reservas.module';
import { AuthService } from './service/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';
import Dexie from 'dexie';
import { datosviaje } from './models/datos_viaje';
import { Observable } from 'rxjs/internal/Observable';


import { flatMap } from 'rxjs/operators';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { from } from 'rxjs';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EspiameDirective } from './espiame.directive';
import { TrackerclickDirective } from './trackerclick.directive';

export interface AppConfig{
  apiEndpoint:String;
}
const APP_CONFIG_VALUE:AppConfig={
apiEndpoint:'http://localhost:3000'
}
export const APP_CONFIG=new InjectionToken<AppConfig>('app.config')
export interface AppState{
  destino:DestinosViajesState
}
const reducers:ActionReducerMap<AppState>={
  destino: reducerDestinosViajes,
};
export function init_app(appLoadService: AppLoadService): () => Promise<any>  {
  return () => appLoadService.intializeDestinosViajesState();
}
@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async intializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
let reducerinits={
  destino:intializeDestinosViajesState()
}
@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  destinos: Dexie.Table<datosviaje, number>;
  translations: any;
  constructor () {
      super('MyDatabase');
 this.version(1).stores({
   destinos: '++id,nombre,imageUrl'
 });
 this.version(2).stores({
  destinos: '++id, nombre, imagenUrl',
  translations: '++id, lang, key, value'
});
    }
  
}
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}
export var db = new MyDatabase();
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => {
                                        if (results.length === 0) {
                                          return this.http
                                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                                            .toPromise()
                                            .then(apiResults => {
                                              db.translations.bulkAdd(apiResults);
                                              return apiResults;
                                            });
                                        }
                                        return results;
                                      }).then((traducciones) => {
                                        console.log('traducciones cargadas:');
                                        console.log(traducciones);
                                        return traducciones;
                                      }).then((traducciones) => {
                                        return traducciones.map((t) => ({ [t.key]: t.value}));
                                      
                                      });
                                      return from(promise).pipe(flatMap((elems:any) => from(elems)));
                                      /*
    return from(promise).pipe(
      map((traducciones) => traducciones.map((t) => { [t.key]: t.value}))
    );
    */
   
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DestinoviajeComponent,
    MenuComponent,
    NuevoComponent,
    FormnuevoComponent,
    LoginComponent,
    ProtectedComponent,
    VueloscomponentComponent,
    VuelosMainComponentComponent,
    VuelosMasinfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TrackerclickDirective,
   

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  RouterModule.forRoot(routes),
  RouterModule.forChild(childrenRoutesVuelos),
  HttpClientModule,
   FormsModule,
   ReactiveFormsModule,
   NgxMapboxGLModule,
   BrowserAnimationsModule,

   NgRxStoreModule.forRoot(reducers,{initialState:reducerinits, runtimeChecks:{
     strictStateImmutability:false,
     strictActionImmutability:false
   }},
    ),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
      }
    }),
  EffectsModule.forRoot([DestinosViajesEffects]),
  CommonModule,
  ReservasModule  ],
  providers:[AuthService,UsuarioLogueadoGuard,{provide:APP_CONFIG,useValue:APP_CONFIG_VALUE}, AppLoadService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
