import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent {
  constructor(public translate: TranslateService) {
    console.log('***************** get translation');
    translate.getTranslation('en').subscribe(x => console.log('x: ' + JSON.stringify(x)));
    translate.setDefaultLang('es');
  }
  title = 'proyectoangula1';
  time=new Observable(observer =>{ setInterval(()=>observer.next(new Date().toString()))});
}
