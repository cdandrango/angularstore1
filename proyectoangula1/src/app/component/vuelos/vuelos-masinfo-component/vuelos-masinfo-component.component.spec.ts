import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VuelosMasinfoComponentComponent } from './vuelos-masinfo-component.component';

describe('VuelosMasinfoComponentComponent', () => {
  let component: VuelosMasinfoComponentComponent;
  let fixture: ComponentFixture<VuelosMasinfoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VuelosMasinfoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VuelosMasinfoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
