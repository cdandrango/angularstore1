import { state } from '@angular/animations';
import { IfStmt } from '@angular/compiler';
import { Component, OnInit, Output,EventEmitter  } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { BorradoAction, ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destino_static';
import { NuevoComponent } from '../nuevo/nuevo.component';
import {datosviaje} from '../../models/datos_viaje'
import {DestinosApiClient} from '../../models/destino_client'
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [DestinosApiClient]

})
export class HeaderComponent implements OnInit {
 searchResult:string[];
 @Output() onItemAdded:EventEmitter<datosviaje>;
 update:string[];
 all;
  constructor(public destinos:DestinosApiClient, private store:Store<AppState>) { 
    this.onItemAdded=new EventEmitter();
    this.update=[];
    this.store.select(state=>state.destino.favorito)
    .subscribe(d=>{
      const fav=d;
      if(d != null){
        this.update.push("Se ha elegido "+ d.nombre);
      {

      }
      }
   store.select(state=>state.destino.items).subscribe(items=>this.all=items)
    });
}

  ngOnInit(): void {
   
  }
agregado(d:datosviaje){
this.destinos.add(d);
this.onItemAdded.emit(d);

}

favorite(e:datosviaje){
//this.datos.forEach(function(x){x.setSelect(false)});
//dato.setSelect(true);
this.destinos.elegir(e)

}
borrado(d:datosviaje){
  
}
getAll(){

}
}
