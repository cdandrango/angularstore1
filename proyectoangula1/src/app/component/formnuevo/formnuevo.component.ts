
import { Component, OnInit, Output ,EventEmitter, inject, forwardRef, Inject} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import {datosviaje} from '../../models/datos_viaje';
import { debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse} from 'rxjs/ajax';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';
@Component({
  selector: 'app-formnuevo',
  templateUrl: './formnuevo.component.html',
  styleUrls: ['./formnuevo.component.scss']
})
export class FormnuevoComponent implements OnInit {
@Output()onItemAdded:EventEmitter<datosviaje>
fg:FormGroup;  
minlong:number=5;
search:string[];
constructor(fg:FormBuilder,@Inject(forwardRef(()=> APP_CONFIG)) private AppConfi:AppConfig) {
this.onItemAdded=new EventEmitter();
this.fg=fg.group({
nombre:['',Validators.compose([Validators.required,this.validacions(this.minlong)])],
url:['']
});

 }

  ngOnInit(): void {
    let element=<HTMLInputElement>document.getElementById('nombre');
    fromEvent(element,'input')
    .pipe(
      map((e:KeyboardEvent)=>((e.target as HTMLInputElement) ).value),
      filter(text=>text.length>3),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text:string)=>ajax(this.AppConfi.apiEndpoint +'/ciudades?q='+ text))
    ).subscribe(ajaxResponse=> { this.search=ajaxResponse.response}
      
      );
    ;
    
  }
guardar(nombre:string,url:string):boolean{
const d=new datosviaje(nombre,url);
this.onItemAdded.emit(d);
return false;
}
getvalidation(control:FormControl):{[s:string]:boolean} {
const i=control.value.toString().trim().length;
if (i>0 && i<=5){
  return{invalidNombre:true};
  
}

return null;
}
validacions(minlong:number):ValidatorFn{
  return(control:FormControl):{[s:string]:boolean} | null =>{
    const i=control.value.toString().trim().length;
    
if (i>0 && i<=minlong){
  return{minlongitudnombre:true};
 
}
console.log(i);
return null;
  }
}
  

}
