import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinosApiClient } from 'src/app/models/destino_client';
import { datosviaje } from 'src/app/models/datos_viaje';
import { identifierModuleUrl } from '@angular/compiler';
class DestinoApiclientViejo{
  getById(id:String):datosviaje{
    console.log("llamada vieja")
    return null
  }
}

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.scss'],
  providers:[DestinosApiClient,{provide:DestinoApiclientViejo,useExisting:DestinosApiClient}]
})


 

export class NuevoComponent implements OnInit {
  destino:datosviaje;
  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };
  constructor(private route: ActivatedRoute, public destinosApiClient:DestinosApiClient) { 
   
    

  }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
  }

}
