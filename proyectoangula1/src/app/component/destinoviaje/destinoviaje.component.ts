import { Component, OnInit,Input, HostBinding, Output ,EventEmitter} from '@angular/core';
import { Store } from '@ngrx/store';
import { Reset, Votonegativo, Votopositivo } from '../../models/destino_static';

import {datosviaje} from '../../models/datos_viaje';
import { animate, state, style, transition, trigger } from '@angular/animations';
@Component({
  selector: 'app-destinoviaje',
  templateUrl: './destinoviaje.component.html',
  styleUrls: ['./destinoviaje.component.scss'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})
export class DestinoviajeComponent implements OnInit {
@Input() datos:datosviaje;
@HostBinding('attr.class') cssClass='col-lg-4 col-md-4 col-sm-6 col-12 mt-4';
@Output() clicked:EventEmitter<datosviaje>;
@Input() posicion:number;

constructor(public store:Store) {
this.clicked=new EventEmitter();

}

  ngOnInit(): void {
    
  }
ir(){
  this.clicked.emit(this.datos);
  return false
}
votopositivo(){
this.store.dispatch(new Votopositivo(this.datos));
}
votonegativo(){
  this.store.dispatch(new Votonegativo(this.datos));
}
reset(){
  this.store.dispatch(new Reset(this.datos)) 
}
}
